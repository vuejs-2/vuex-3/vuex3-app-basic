export default {
    namespaced: true,
    state: {
        tasks: ['Task1', 'Task2', 'Task3', 'Task4'],
        amountTasks: null,
    },
    mutations: {
        countTasks(state){
            state.amountTasks = state.tasks.length
        }
    }
}