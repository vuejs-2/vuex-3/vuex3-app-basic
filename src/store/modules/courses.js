import { GETCOURSES_MUTATION, ADDCOURSE_MUTATION } from "../mutation-types";

export default {
    namespaced: true,
    state: {
        courseName: '',
        courses: [
           {
                id: 1,
                name: "HTML"
           } 
        ]
    },
    mutations: {
        [ GETCOURSES_MUTATION ] (state, coursesAction) {
            state.courses = coursesAction
            console.log(state.courses)
        },
        [ADDCOURSE_MUTATION] (state, payload) {
            state.courses.push({
                name: payload
            })
            console.log("payload:" +payload);
        },
        updateCourseName(state, payload) {
            state.courseName = payload
        }
    },
    actions: {
        async getCourses({ commit }) {
            const data = await fetch('courses.json')
            const coursesAction = await data.json()
            commit('GETCOURSES_MUTATION', coursesAction)
        }
    }
}