import Vue from 'vue'
import Vuex from 'vuex'

import tasks from './modules/tasks'
import courses from './modules/courses'
import memes from './modules/memes'
import { INCREMENT_MUTATION, DECREMENT_MUTATION } from './mutation-types'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    count: 0
  },
  getters: {
  },
  mutations: {
    [INCREMENT_MUTATION] (state) {
      state.count++
      console.log('sumando');
    },
    [DECREMENT_MUTATION] (state) {
      state.count--
    }
  },
  actions: {
    
  },
  modules: {
    tasks,
    courses,
    memes
  }
})
